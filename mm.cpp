/*
 * Popis: Algoritmus mesh multiplication pro násobení matic
 * Autor: Jan Herec, xherec00
 * Kódování: UTF-8
 */

 #include <mpi.h>
 #include <iostream>
 #include <fstream>
 #include <deque>
 #include <string>
 #include <string.h>
 #include <stdlib.h>
 #include <sstream>
 #include <vector>
 #include <stdexcept>  

 using namespace std;

 #define TAG 0   // základní tag komunikace
 #define FALSE 0
 #define TRUE 1

 /**
  * Funkce rozdělí řetězec na vstupu podle oddělovače
  * @param strToSplit řetězec, který se rozdělí podle oddělovače
  * @param delimiter oddělovač, podle kterého se rozdělí řězetec
  * @return vektor dílčích řetězců rozdělených podle oddělovače
  */
 vector<string> splitByDelimiter(string strToSplit, char delimiter) {

    stringstream strStream; 
    strStream << strToSplit; // vytvoříme z řetezce string stream
    string strChunk;         // dílčí řetězec oddělený oddělovačem
    vector<string> vectorOfSplittedStrings; // vektor dílčích řětězců

    // postupně získáváme dílčí řetezce a ukládáme do vektoru dílčích řětezců
    while (getline(strStream, strChunk, delimiter)) {
        vectorOfSplittedStrings.push_back(strChunk);
    }
    return vectorOfSplittedStrings;
 }

 /**
  * Funkce ukončí daný proces s chybou,liší se pro proces s rankem 0 a ostatní
  * @param rank rank procesu, který má být ukončen
  * @return 1
  */
 int exitWithError(int rank) {
    if (rank == 0) {
        int errorDuringCheckMatrix = TRUE;
        MPI_Bcast(&errorDuringCheckMatrix, 1, MPI_INT, 0, MPI_COMM_WORLD);
        MPI_Finalize(); 
        return 1;
    }
    else {
        MPI_Finalize(); 
        return 1;    
    }
 }

/**
 * Klasická funkce main, která řeší v podstatě celý výpočet paralelního násobení matic
 */
 int main(int argc, char *argv[])
 {
    int numprocs;          // pocet procesoru
    int myid;              // muj rank
    MPI_Status stat;       // struct- obsahuje kod- source, tag, error

    double start, finish;  // Měření složitosti algoritmu

    int result_matrix_rows; // počet řádků výstupní matice
    int result_matrix_cols; // počet sloupců výstupní matice
    vector<int> inputItems; // pole vstupních prvků pro okrajové procesory (u procesoru s rankem 0 jsou to levé vstupy)
    vector<int> inputItems2; // 2. pole vstupních prvků pro okrajový procesor s rankem 0 (horní vstupy)

    int c; // registr c uchovávající výsledný prvek ve výstupní matici
    int a; // prvek z 1. matice (matice A) přijatý procesorem od jeho levého souseda, nebo v případě okrajového procesoru přímo získaný z matice A
    int b;  // prvek z 2. matice (matice B) přijatý procesorem od jeho horního souseda, nebo v případě okrajového procesoru přímo získaný z matice B
    int receive_a; // TRUE (bude přijat prvek procesem od jeho levého souseda) nebo FALSE (nebude přijat prvek procesem od jeho levého souseda, tzn. prvky z daného řádku matice již byly vyčerpány)   
    int receive_b; // TRUE (bude přijat prvek procesem od jeho horního souseda) nebo FALSE (nebude přijat prvek procesem od jeho horního souseda, tzn. prvky z daného sloupce matice již byly vyčerpány)

    int Mat1rows; // počet řádků vstupní matice A - mat1
    int Mat1cols; // počet sloupců vstupní matice A mat1

    int Mat2rows; // počet řádků vstupní matice B - mat2
    int Mat2cols; // počet sloupců vstupní matice B mat2

    int **mat1; // vstupní matice 1 (A)
    int **mat2; // vstupní matice 2 (B)

    int errorDuringCheckMatrix; // příznak jestli nastala chyba při ověřování vstupních matic procesorem s rankem 0

    //MPI INIT
    MPI_Init(&argc,&argv);                          // inicializace MPI 
    MPI_Comm_size(MPI_COMM_WORLD, &numprocs);       // zjistíme, kolik procesů běží 
    MPI_Comm_rank(MPI_COMM_WORLD, &myid);           // zjistíme id svého procesu 

    receive_a = TRUE; // defaultně bude přijat prvek procesem od jeho levého souseda
    receive_b = TRUE; // defaultně bude přijat prvek procesem od jeho horního souseda
    c = 0; // inicilaizace registru c neutrálním prvkem vzhledem ke sčítání - tedy nulou
    errorDuringCheckMatrix = FALSE; // defaultně chyba nenastala

    // ************ FÁZE 1: připravení vstupů *************************
    
    // procesor s rankem 0 načte obě matice, zkontroluje je, a uloží si do pole inputItems a inputItems2 prvky matic, které bude ve fázi výpočtu postupně načítat a zasílat svým sousedům
    if (myid == 0) {

        string line;                  // načtený řádek ze vstupního souboru
        vector<string> splittedLine;  // načtený řádek ze vstupního souboru rozdělený na části podle mezery
  
        try {
                        
            // ----- NYNI BUDU NACITAT MATICI 1 ZE VSTUPU ------------
        
            ifstream mat1Stream("mat1", ios::in); // otevřeme soubor mat1, který obsahuje matici 1 (A)

            // určíme počet řádků matice 1 podle čísla na prvním řádku souboru mat1
            getline(mat1Stream, line);   
            Mat1rows = stoi(line); 

            // určíme počet sloupců matice 1 podle počtu prvků prvního řádku matice
            getline(mat1Stream, line); 
            splittedLine = splitByDelimiter(line, ' ');
            Mat1cols = splittedLine.size();
            
            // ověříme jestli má matice smysluplný počet řádků a sloupců
            if (Mat1rows == 0 || Mat1cols == 0) {
                cerr << "Chyba: matice 1 nemůže mít nulový počet řádků nebo sloupců." << endl << flush;
                return exitWithError(myid);
            }

            // znám rozměry matice 1, mohu pro ni alokovat prostor
            mat1 = new int*[Mat1rows];
            for (int i = 0; i < Mat1rows; i++) {
                mat1[i] = new int[Mat1cols]; 
            }

            // načtu matici 1 ze vstupu
            for (int row = 0; row < Mat1rows; row++) {
                // načtu sloupce pro daný řádek
                splittedLine = splitByDelimiter(line, ' ');
                
                // ověříme, jestli sedí počet sloupců na aktuálně načteném řádku
                if (splittedLine.size() != Mat1cols) {
                    cerr << "Chyba: matice 1 má nestejný počet sloupců pro řádky." << endl << flush;
                    return exitWithError(myid);
                }

                // uložím načtený řádek do matice po sloupcích
                for (int col = 0; col < Mat1cols; col++) {
                    // pokusím se prvek matice převést na číslo
                    try {
                        mat1[row][col] = stoi(splittedLine[col]);
                    } catch (const std::invalid_argument& ia) {
                        cerr << "Chyba: matice 1 obsahuje prvek, který není číslo." << endl << flush;
                        return exitWithError(myid);
                    }
                }
                
                // přečteme řádek ze vstupu a zjistíme ejstli se nám to podařilo
                bool retVal = false;
                if (getline(mat1Stream, line)) retVal = true; 
                
                // pokud dostanu chybu až v posledni iteraci je to v poradku, ale jinak nee, protože má matice méně řádku než je udáno
                if (!retVal && row + 1 != Mat1rows) {
                    cerr << "Chyba: matice 1 má méně řádků než je deklarovaných." << endl << flush;
                    return exitWithError(myid);
                }
                // za posledním řádkem matice není EOF, ale znak konce řádku, takže jsme nyní mohli načíst další řádek, pokud ale tento není ukončen rovnou EOF, ale něco obsahuje, tak má matice více řádku než je udáno
                else if (retVal && row + 1 == Mat1rows && line != "") {
                    cerr << "Chyba: matice 1 má více řádků než je deklarovaných." << endl << flush;
                    return exitWithError(myid);
                }
            }

            mat1Stream.close();

            // ------ NYNI BUDU NACITAT MATICI 2 ZE VSTUPU ---------

            ifstream mat2Stream("mat2", ios::in); // otevřeme soubor mat2, který obsahuje matici 2 (B)

            // určíme počet řádků matice 2 podle sloupců matice 1
            Mat2rows = Mat1cols;

            // určíme počet sloupců matice 2 podle čísla na prvním řádku souboru mat2
            getline(mat2Stream, line);
            Mat2cols = stoi(line);
            
            // ověříme jestli má matice smysluplný počet sloupců (řádky jsme převzali podle sloupců z matice 1, kterou jsme již ověřili)
            if (Mat2cols == 0) {
                cerr << "Chyba: matice 2 nemůže mít nulový počet sloupců." << endl << flush;
                return exitWithError(myid);
            }

            // znám rozměry matice 2, mohu pro ni alokovat prostor
            mat2 = new int*[Mat2rows];
            for (int i = 0; i < Mat2rows; i++) {
                mat2[i] = new int[Mat2cols]; 
            }

            // načtu matici 2 ze vstupu
            for (int row = 0; row < Mat2rows; row++) {
                
                // pokud dostanu chybu v jakekoliv iteraci má matice 2 méně řádku než je je potřeba (kvůli matici 1)
                if (!getline(mat2Stream, line)) {
                    cerr << "Chyba: matice 2 má méně řádků než je potřeba pro násobení s maticí 1." << endl << flush;
                    return exitWithError(myid);
                }
                // načtu sloupce pro daný řádek
                splittedLine = splitByDelimiter(line, ' ');
                // ověříme, jestli sedí počet sloupců na aktuálně načteném řádku
                if (splittedLine.size() != Mat2cols) {
                    cerr << "Chyba: matice 2 má alespoň v jednom řádku jiný počet sloupců, než je deklarovaných." << endl << flush;
                    return exitWithError(myid);
                }
                
                // uložím načtený řádek do matice po sloupcích
                for (int col = 0; col < Mat2cols; col++) {
                    // pokusím se prvek matice převést na číslo
                    try {
                        mat2[row][col] = stoi(splittedLine[col]); 
                    } catch (const std::invalid_argument& ia) {
                        cerr << "Chyba: matice 2 obsahuje prvek, který není číslo." << endl << flush;
                        return exitWithError(myid);
                    }
                }
                
            }
            // za posledním řádkem matice není EOF, ale znak konce řádku, takže jsme nyní mohli načíst další řádek, pokud ale tento není ukončen rovnou EOF, ale něco obsahuje, tak má matice více řádku než je potřeba pro násobení s maticí 1 a to se nám nelíbí
            if (getline(mat2Stream, line)) {
                if (line != "") {
                    cerr << "Chyba: matice 2 má více řádků než je potřeba pro násobení s maticí 1." << endl << flush;
                    return exitWithError(myid);
                }
            }

            mat2Stream.close();

            // PO NAČTENÍ OBOU MATIC PROVEDU POSTPROCESSING 
            
            // načtu si první řádek matice 1, který bude tvořit levé vstupní prvky procesoru s rankem 0
            for (int col = 0; col < Mat1cols; col++) {
                inputItems.push_back(mat1[0][col]);
            }


            // načtu si první sloupec matice 2, který bude tvořit horní vstupní prvky procesoru s rankem 0
            for (int row = 0; row < Mat2rows; row++) {
                inputItems2.push_back(mat2[row][0]);
            }

            // zjistíme rozměry výsledné matice
            result_matrix_rows = Mat1rows;
            result_matrix_cols = Mat2cols;

            // uvolnim matici 1
            for (int i = 0; i < Mat1rows; i++) {
                delete [] mat1[i]; 
            }
            delete [] mat1; 

            // uvolnim matici 2
            for (int i = 0; i < Mat2rows; i++) {
                delete [] mat2[i]; 
            }
            delete [] mat2; 
        
        }
        // odchyteme výjimky
        catch (const std::exception &e)
        {
            cerr << e.what() << endl << flush;  
            MPI_Abort(MPI_COMM_WORLD, 1);
        }
        catch (...)
        {
            cerr << "Nastala neznámá výjimka..." << endl << flush;
            MPI_Abort(MPI_COMM_WORLD, 1);
        }       

    }

    // proces s rankem 0 zašle info všem ostatním proesorů, že chyba nenastala (kdyby nastala, tak se do této části kódu nedostane, ale broadcastem zašle info o chybě mnohem dříve)
    if (myid == 0) {
        MPI_Bcast(&errorDuringCheckMatrix, 1, MPI_INT, 0, MPI_COMM_WORLD);
    }
    // procesory které nemají rank 0 nejdříve získají informaci od procesu s rankem 0, jestli nedošlo k chybě v procesu ranku 0, když kontroloval matice a případně se korektně ukončí
    if (myid > 0) {
        MPI_Bcast(&errorDuringCheckMatrix, 1, MPI_INT, 0, MPI_COMM_WORLD);
        if (errorDuringCheckMatrix == TRUE) return exitWithError(myid);
    }

    // procesor s rankem 0 zašle počet řádků výsledné matice broadcastem všem procesorům
    MPI_Bcast(&result_matrix_rows, 1, MPI_INT, 0, MPI_COMM_WORLD);

    // procesor s rankem 0 zašle počet řádků druhé matice  broadcastem všem procesorům
    MPI_Bcast(&Mat2rows, 1, MPI_INT, 0, MPI_COMM_WORLD);

    // a procesory které obdrželi počet řádků výsledné matice si na základě počtu procesorů a počtu řádků výsledné matice dopočítají počet sloupců výsledné matice
    if (myid > 0) {
        result_matrix_cols = numprocs / result_matrix_rows;
    }

    // okrajové levé procesory, kromě procesoru s rankem 0 načtou matici 1 
    if (myid != 0 && myid % result_matrix_cols == 0) {


            try {
                string line;                  // načtený řádek ze vstupního souboru
                vector<string> splittedLine;  // načtený řádek ze vstupního souboru rozdělený na části podle mezery

                // ----- NYNI BUDU NACITAT MATICI 1 ZE VSTUPU, tentokrát ale už nemusím ověřovat nic, toto už udělal procesor s rankem 0
          
                ifstream mat1Stream("mat1", ios::in);

                // určíme počet řádků matice 1 podle čísla na prvním řádku souboru mat1
                getline(mat1Stream, line); 
                Mat1rows = stoi(line);

                // určíme počet sloupců matice 1 podle počtu čísel prvního řádku matice
                getline(mat1Stream, line); 
                splittedLine = splitByDelimiter(line, ' ');
                Mat1cols = splittedLine.size();

                // znám rozměry matice 1, mohu pro ni alokovat prostor
                mat1 = new int*[Mat1rows];
                for (int i = 0; i < Mat1rows; i++) {
                    mat1[i] = new int[Mat1cols]; 
                }

                // načtu matici 1 ze vstupu
                for (int row = 0; row < Mat1rows; row++) {
                    splittedLine = splitByDelimiter(line, ' ');
                    for (int col = 0; col < Mat1cols; col++) {
                        mat1[row][col] = stoi(splittedLine[col]);
                    }
                    getline(mat1Stream, line);
                }

                mat1Stream.close();

                // načtu si odpovídající řádek řádek matice 1, který bude tvořit levé vstupní prvky tohoto procesu
                for (int col = 0; col < Mat1cols; col++) {
                    inputItems.push_back(mat1[myid / result_matrix_cols][col]);
                }

                // uvolnim matici 1
                for (int i = 0; i < Mat1rows; i++) {
                    delete [] mat1[i]; 
                }
                delete [] mat1; 
            }
            // odchyteme výjimky
            catch (const std::exception &e)
            {
                cerr << e.what() << endl << flush;  
                MPI_Abort(MPI_COMM_WORLD, 1);
            }
            catch (...)
            {
                cerr << "Nastala neznámá výjimka..." << endl << flush;
                MPI_Abort(MPI_COMM_WORLD, 1);
            } 

    }
    // okrajové horní procesory, kromě procesoru s rankem 0 načtou matici 2
    else if (myid != 0 && myid < result_matrix_cols) {

            try {
                string line;                  // načtený řádek ze vstupního souboru
                vector<string> splittedLine;  // načtený řádek ze vstupního souboru rozdělený na části podle mezery

                
                // ------ NYNI BUDU NACITAT MATICI 2 ZE VSTUPU, tentokrát ale už nemusím ověřovat nic, toto už udělal procesor s rankem 0

                ifstream mat2Stream("mat2", ios::in);

                // počet řádků matice 2 Mat2rows, známe, protože nám tuto hodnotu zaslal broadcastem procesor s rankem 0

                // určíme počet sloupců matice 2 podle čísla na prvním řádku souboru mat2
                getline(mat2Stream, line);    
                Mat2cols = stoi(line);  // ověřit na chy

                // znám rozměry matice 2, mohu pro ni alokovat prostor
                mat2 = new int*[Mat2rows];
                for (int i = 0; i < Mat2rows; i++) {
                    mat2[i] = new int[Mat2cols]; 
                }

                // načtu matici 2 ze vstupu
                for (int row = 0; row < Mat2rows; row++) {
                    getline(mat2Stream, line);

                    splittedLine = splitByDelimiter(line, ' ');
                    for (int col = 0; col < Mat2cols; col++) {
                        mat2[row][col] = stoi(splittedLine[col]); 
                    }
                }

                mat2Stream.close();

                // načtu si odpovídající sloupec matice 2, který bude tvořit horní vstupní prvky tohoto procesu
                for (int row = 0; row < Mat2rows; row++) {
                    inputItems.push_back(mat2[row][myid]);
                    
                }

                // uvolnim matici 2
                for (int i = 0; i < Mat2rows; i++) {
                    delete [] mat2[i]; 
                }
                delete [] mat2; 
 
            }
            // odchyteme výjimky
            catch (const std::exception &e)
            {
                cerr << e.what() << endl << flush;  
                MPI_Abort(MPI_COMM_WORLD, 1);
            }
            catch (...)
            {
                cerr << "Nastala neznámá výjimka..." << endl << flush;
                MPI_Abort(MPI_COMM_WORLD, 1);
            } 
    } 
    
    // ************ FÁZE 2: samotný výpočet *************************

    int counter = 0; // u okrajových procesů určuje index prvku, který se bude načítat z vektoru inputItems (popř. inputItems2)

    /* Ověření časové složitosti
    MPI_Barrier(MPI_COMM_WORLD); // synchornizujeme procesy
    if (myid == numprocs - 1) {
        start=MPI_Wtime();
    }*/
    
    // dokud mají procesy co příjímat od sousedů, tak pracují
    while(receive_a != FALSE && receive_b != FALSE) {
        // pracují okrajové levé procesory, kromě procesoru s rankem 0, tyto přijímají prvky matice od horních sousedů (prvek b) a z vektoru inputItems (prvek a)
        if (myid != 0 && myid % result_matrix_cols == 0) {
            // ziskame informaci odostupnostzi prvku z horniho souseda         
            MPI_Recv(&receive_b, 1, MPI_INT, myid - result_matrix_cols, TAG, MPI_COMM_WORLD, &stat); 

            // mame k dispozici prvky z hora, tedy musime je mit i zleva, muzeme zasilat pravému a spodnimu spousedovi 
            if (receive_b == TRUE) {
                receive_a = TRUE;
            }
            else {
                receive_a = FALSE;
            }
            
            // dame vedet sousedum, pokud existují (pravemu a spodnimu) o dstupnosti nebo nedostupnosti prvků 
            if (myid % result_matrix_cols < result_matrix_cols - 1) MPI_Send(&receive_a, 1, MPI_INT, myid + 1, TAG, MPI_COMM_WORLD);  
            if (myid + result_matrix_cols < numprocs) MPI_Send(&receive_b, 1, MPI_INT, myid + result_matrix_cols, TAG, MPI_COMM_WORLD); 
            
            // mame k dispozici prvky, muzeme zasilat pravému a spodnimu spousedovi            
            if (receive_a == TRUE && receive_b == TRUE) {  

                a = inputItems[counter]; // ziskam dany vstupni prvek "a" podle aktuálního počítadla kroků counter  z vektoru inputItems
                MPI_Recv(&b, 1, MPI_INT, myid - result_matrix_cols, TAG, MPI_COMM_WORLD, &stat); // prijmu prvek "b" od horniho souseda

                c += a * b; // vypočítáme průběžný součet pro prvek výstupní matice reprezntovaný tímto procesem
                
                // pošleme sousedum prvky a (pravemu),b (spodnimu), ktere jsme prijali od sousedu
                if (myid % result_matrix_cols < result_matrix_cols - 1) MPI_Send(&a, 1, MPI_INT, myid + 1, TAG, MPI_COMM_WORLD);    
                if (myid + result_matrix_cols < numprocs) MPI_Send(&b, 1, MPI_INT, myid + result_matrix_cols, TAG, MPI_COMM_WORLD);  
            }
        }
        // pracují okrajové horní procesory, kromě procesoru s rankem 0, tyto přijímají prvky matice od levých sousedů (prvek a) a z vektoru inputItems (prvek b)
        else if (myid != 0 && myid < result_matrix_cols) {
            // ziskame informaci odostupnostzi prvku z leveho souseda           
            MPI_Recv(&receive_a, 1, MPI_INT, myid - 1, TAG, MPI_COMM_WORLD, &stat); 

            // mame k dispozici data zleva, tedy musime je mit i zhora, muzeme zasilat pravému a spodnimu spousedovi 
            if (receive_a == TRUE) {
                receive_b = TRUE;
            }
            else {
                receive_b = FALSE;
            }
            
            // dame vedet sousedum, pokud existují (pravemu a spodnimu) o dstupnosti nebo nedostupnosti prvků 
            if (myid % result_matrix_cols < result_matrix_cols - 1) MPI_Send(&receive_a, 1, MPI_INT, myid + 1, TAG, MPI_COMM_WORLD);  
            if (myid + result_matrix_cols < numprocs) MPI_Send(&receive_b, 1, MPI_INT, myid + result_matrix_cols, TAG, MPI_COMM_WORLD); 
            
            // mame k dispozici prvky, muzeme zasilat pravému a spodnimu spousedovi     
            if (receive_a == TRUE && receive_b == TRUE) {  

                b = inputItems[counter]; // ziskam dany vstupni prvek "a" podle aktulíního počítadla kroků counter z vektoru inputItems
                MPI_Recv(&a, 1, MPI_INT, myid - 1, TAG, MPI_COMM_WORLD, &stat);  // prijmu prvek b od horniho souseda

                c += a * b; // vypočítáme průběžný součet pro prvek výstupní matice reprezntovaný tímto procesem
                
                // pošleme sousedum prvky a (pravemu),b (spodnimu), ktere jsme prijali od sousedu
                if (myid % result_matrix_cols < result_matrix_cols - 1) MPI_Send(&a, 1, MPI_INT, myid + 1, TAG, MPI_COMM_WORLD);    
                if (myid + result_matrix_cols < numprocs) MPI_Send(&b, 1, MPI_INT, myid + result_matrix_cols, TAG, MPI_COMM_WORLD);  
            }
        }
        // pracuje procesor s rankem 0 (levý horní)
        else if (myid == 0) {
            
            // dokud máme vstupní prvky z vektorů inputItems, respektive inputItems2
            if (counter < inputItems.size()) {
                receive_a = TRUE;
                receive_b = TRUE;
            }
            else {
                receive_a = FALSE;
                receive_b = FALSE;
            }
            
            // dame vedet sousedum, pokud existují (pravemu a spodnimu) o dstupnosti nebo nedostupnosti prvků 
            if (myid % result_matrix_cols < result_matrix_cols - 1) MPI_Send(&receive_a, 1, MPI_INT, myid + 1, TAG, MPI_COMM_WORLD);  
            if (myid + result_matrix_cols < numprocs) MPI_Send(&receive_b, 1, MPI_INT, myid + result_matrix_cols, TAG, MPI_COMM_WORLD); 
            
            
            // mame k dispozici prvky, muzeme zasilat pravému a spodnimu spousedovi            
            if (receive_a == TRUE && receive_b == TRUE) {  

                a = inputItems[counter];  // ziskam dany vstupni prvek "a" podle aktulíního počítadla kroků counter z vektoru inputItems
                b = inputItems2[counter]; // ziskam dany vstupni prvek "b" podle aktulíního počítadla kroků counter z vektoru inputItems2

                c += a * b; // vypočítáme průběžný součet pro prvek výstupní matice reprezntovaný tímto procesem
                
                // pošleme sousedum prvky a (pravemu),b (spodnimu), ktere jsme ziskali ze vstupních matic
                if (myid % result_matrix_cols < result_matrix_cols - 1) MPI_Send(&a, 1, MPI_INT, myid + 1, TAG, MPI_COMM_WORLD);    
                if (myid + result_matrix_cols < numprocs) MPI_Send(&b, 1, MPI_INT, myid + result_matrix_cols, TAG, MPI_COMM_WORLD);  
            } 
        }
        // pracují nekrajní (ne levé, ne horní) procesory
        else {
            // ziskame informaci odostupnostzi prvku z leveho souseda       
            MPI_Recv(&receive_a, 1, MPI_INT, myid - 1, TAG, MPI_COMM_WORLD, &stat); 
            // ziskame informaci odostupnostzi prvku z horniho souseda       
            MPI_Recv(&receive_b, 1, MPI_INT, myid - result_matrix_cols, TAG, MPI_COMM_WORLD, &stat); 

            // dame vedet sousedum, pokud existují (pravemu a spodnimu) o dstupnosti nebo nedostupnosti prvků 
            if (myid % result_matrix_cols < result_matrix_cols - 1) MPI_Send(&receive_a, 1, MPI_INT, myid + 1, TAG, MPI_COMM_WORLD);  
            if (myid + result_matrix_cols < numprocs) MPI_Send(&receive_b, 1, MPI_INT, myid + result_matrix_cols, TAG, MPI_COMM_WORLD); 
            
            // mame k dispozici prvky, muzeme zasilat pravému a spodnimu sousedovi            
            if (receive_a == TRUE && receive_b == TRUE) {  

                MPI_Recv(&b, 1, MPI_INT, myid - result_matrix_cols, TAG, MPI_COMM_WORLD, &stat);    // prijmu prvek "b" od horniho souseda
                MPI_Recv(&a, 1, MPI_INT, myid - 1, TAG, MPI_COMM_WORLD, &stat);    // prijmu prvek "a" od leveho souseda

                c += a * b;  // vypočítáme průběžný součet pro prvek výstupní matice reprezntovaný tímto procesem
                
                // pošleme sousedum prvky a (pravemu),b (spodnimu), ktere jsme prijali od sousedu
                if (myid % result_matrix_cols < result_matrix_cols - 1) MPI_Send(&a, 1, MPI_INT, myid + 1, TAG, MPI_COMM_WORLD);    
                if (myid + result_matrix_cols < numprocs) MPI_Send(&b, 1, MPI_INT, myid + result_matrix_cols, TAG, MPI_COMM_WORLD);  
            }
        }

        counter++; // zvysime index do vektorů inputItems, inputItems2, tedy okrajové procesory načtou z matice další její prvky
    }
    
    /* Ověření časové složitosti
    if (myid == numprocs - 1) {
        finish=MPI_Wtime();
        printf("%f\n",(finish-start)*1000); 
    }*/

    // vypíšeme výslednou matici procesorem s rankem 0
    if (myid == 0) {
        // nejdříve si své vypíše procesor s rankem 0
        cout << result_matrix_rows << ":" << result_matrix_cols << endl;
        if (myid % result_matrix_cols == result_matrix_cols - 1) {
             cout << c << endl << flush;  
        }
        else {
             cout << c << " " << flush;  
        }

        // poté procesor s rankem 0 přijme od všech procesorů jejich prvky matice a vypíše je na výstup
        for (int i = 1; i < numprocs; i++) {
            MPI_Recv(&c, 1, MPI_INT, i, TAG, MPI_COMM_WORLD, &stat); 
            
            if (i % result_matrix_cols == result_matrix_cols - 1) {
                 cout << c << endl << flush;  
            }
            else {
                 cout << c << " " << flush;  
            }

        }
    }
    // procesory zasílají své prvky matice procesoru s rankem 0 
    else {
        MPI_Send(&c, 1, MPI_INT, 0, TAG, MPI_COMM_WORLD);    
    }
 
    MPI_Finalize(); 

    return 0;

 }//main

